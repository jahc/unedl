<!DOCTYPE html>
<html lang="en">
                <!--CABECERA-->
                    <head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                        <title>Universidad Enrique Díaz de León</title>
                        <link rel="stylesheet" href="css/materialdesignicons.min.css">
                        <link rel="stylesheet" href="css/vendor.bundle.base.css">
                        <link rel="stylesheet" href="css/style.css">
                        <link rel="shortcut icon" href="img/LogoUnedl.png" />
                    </head>
                <!--ENDCABECERA-->
    <body>
            <div class="horizontal-menu">
                <!--MENUCABECERA-->
                    <nav class="navbar top-navbar col-lg-12 col-12 p-0">
                            <div class="container-fluid">
                                <div class="navbar-menu-wrapper d-flex align-items-center justify-content-between">
                                    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                                        <a class="navbar-brand brand-logo" href="{{ url('menu') }}"><img src="img/logo_unedl.png" alt="logo"/></a>
                                    </div>
                                    <ul class="navbar-nav navbar-nav-right">
                                        <li class="nav-item nav-profile dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                                            <span class="nav-profile-name">UNEDL</span>
                                            <img src="img/LogoUnedl.png" alt="profile"/>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                                            <a class="dropdown-item">
                                                <i class="mdi mdi-logout text-primary"></i>
                                                Salida
                                            </a>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                    </nav>
                <!--ENDMENUCABECERA-->
                <!--MENU-->
                <nav class="bottom-navbar">
                            <div class="container">
                                <ul class="nav page-navigation">
                                                    <!--Coordinadora-->
                                                        <li class="nav-item">
                                                            <a href="#" class="nav-link">
                                                                <i class="mdi mdi-cube-outline menu-icon"></i>
                                                                <span class="menu-title">Coordinadora</span>
                                                                <i class="menu-arrow"></i>
                                                            </a>
                                                            <div class="submenu">
                                                                <ul>
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('altaUsuarios') }}">Alta de Usuarios</a></li>
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('asignacionMaterias') }}">Asignación de Materias</a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    <!--Coordinadora-->
                                                    <!--Turno-->
                                                        <li class="nav-item">
                                                            <a href="#" class="nav-link">
                                                                <i class="mdi mdi-cube-outline menu-icon"></i>
                                                                <span class="menu-title">Turnos</span>
                                                                <i class="menu-arrow"></i>
                                                            </a>
                                                            <div class="submenu">
                                                                <ul>
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('matutino') }}">Matutino</a></li>
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('vespertino') }}">Vespertino</a></li>
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('mixto') }}">Mixto</a></li>
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('nocturno') }}">Nocturno</a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    <!--Turno-->
                                                    <!--Maestros-->
                                                        <li class="nav-item">
                                                            <a href="#" class="nav-link">
                                                                <i class="mdi mdi-codepen menu-icon"></i>
                                                                <span class="menu-title">Maestros</span>
                                                                <i class="menu-arrow"></i>
                                                            </a>
                                                            <div class="submenu">
                                                                <ul class="submenu-item">
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('maestros1') }}">Maestros 1</a></li>
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('maestros2') }}">Maestros 2</a></li>
                                                                    <li class="nav-item"><a class="nav-link" href="{{ url('maestros3') }}">Maestros 3</a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    <!--Maestros-->
                                </ul>
                            </div>
                </nav>
                <!--ENDMENU-->
            </div>          
                <!--CONTENIDO-->
                    <div class="content-wrapper">   
                        @yield('seccion') 
                    </div>
                <!--ENDCONTENIDO-->
                <!--FOOTER-->
                    <footer class="footer">
                        <div class="footer-wrap">
                                <div class="w-100 clearfix">
                                    <span class="d-block text-center text-sm-left d-sm-inline-block">JalOax</span>
                                </div>
                        </div>
                    </footer>
                <!--ENDFOOTER-->
                <!--JAVASCRIPT-->
                    <script src="js/vendor.bundle.base.js"></script>
                    <script src="js/template.js"></script>
                    <script src="js/Chart.min.js"></script>
                    <script src="js/progressbar.min.js"></script>
                    <script src="js/chartjs-plugin-datalabels.js"></script>
                    <script src="js/raphael-2.1.4.min.js"></script>
                    <script src="js/justgage.js"></script>
                    <script src="js/dashboard.js"></script>
                <!--ENDJAVASCRIPT-->
    </body>
</html>