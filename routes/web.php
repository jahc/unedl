<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/menu', function () {
    return view('menu');
});

//COORDINADORA//
Route::get('/altaUsuarios', function () {
    return view('coordinadora.altaUsuarios');
});

Route::get('/asignacionMaterias', function () {
    return view('coordinadora.asignacionMaterias');
});
//ENDCOORDINADORA//

//TURNOS//
Route::get('/matutino', function () {
    return view('turnos.matutino');
});

Route::get('/vespertino', function () {
    return view('turnos.vespertino');
});

Route::get('/mixto', function () {
    return view('turnos.mixto');
});

Route::get('/nocturno', function () {
    return view('turnos.nocturno');
});
//ENDTURNOS//

//MAESTROS
Route::get('/maestros1', function () {
    return view('maestros.maestros1');
});

Route::get('/maestros2', function () {
    return view('maestros.maestros2');
});

Route::get('/maestros3', function () {
    return view('maestros.maestros3');
});
//ENDMAESTROS//